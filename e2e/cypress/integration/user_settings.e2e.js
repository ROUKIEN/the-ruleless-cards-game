describe('User settings', () => {
  beforeEach(() => {
    cy.clearLocalStorage()
  })

  context('First visit', () => {
    it('user has a default username', () => {
      cy.visit('/')

      cy.dataTest('user-username')
        .contains(/player/)
    })
  })

  context('Nth visit', () => {
    beforeEach(() => {
      const user = {
        id: '11bf5b37-e0b8-42e0-8dcf-dc8c4aefc000',
        username: 'Morgoth',
        color: '#FF0000'
      }
      localStorage.setItem('user', JSON.stringify(user))
    })

    it('user has the same username & color as from previous session', () => {
      cy.visit('/')

      cy.dataTest('user-username')
        .contains(/Morgoth/)
    })
  })

  it('updates the player username', () => {
    cy.viewport(1500, 768) // otherwise I had to expand the menu bar but this is not what I wanna test here
    cy.visit('/')

    cy.dataTest('user-username')
      .click()

    cy.dataTest('update-username')
      .clear()
      .type('Morgoth')
    
    cy.get('.modal-card-foot')
      .find('button.is-primary')
      .click()
      .should(() => {
        expect(localStorage.getItem('user')).to.contain('Morgoth')
      })
  })

  it('updates the player color', () => {
    cy.viewport(1500, 768) // otherwise I had to expand the menu bar but this is not what I wanna test here
    cy.visit('/')

    cy.dataTest('user-color')
      .click()
    
    cy.dataTest('user-color')
      .find('.vue-swatches__swatch[aria-label="#C0382B"]')
      .click()
      .should(() => {
        expect(localStorage.getItem('user')).to.contain('#C0382B')
      })
  })
})