describe('Room joining', () => {
  it('can join an existing by filling a room ID', () => {
    cy.createRoom()
      .then(room => {
        cy.visit('/join')

        cy.dataTest('join-room-id')
          .type(`${room.id}{enter}`)

        cy.location().should(loc => {
          expect(loc.pathname).to.eq(`/room/${room.id}`)
        })
      })
    })

  context('First time joining a room', () => {
    let room = null
    beforeEach(() => {
      cy.clearLocalStorage()
      cy.createRoom()
        .then(theRoom => {
          room = theRoom
        })
    })
    
    it('renders a welcome screen', () => {
      cy.visit(`/room/${room.id}`)

      cy.dataTest('modal-how-to-play').should('be.visible')
    })
  })
})