describe('Room creation', () => {
  it('Creates a simple room', () => {
    cy.visit('/new')

    cy.dataTest('input-name').type('My room for testing{enter}')

    cy.location().should(loc => {
      expect(loc.pathname).to.contain(/room/)
    })
  })

  it('Creates a room with a custom deck', () => {
    cy.visit('/new')
    
    cy.dataTest('input-name').type('Room room 64 cards unshuffled deck')
    cy.dataTest('form-more-options-button').click()

    cy.dataTest('input-nb-deck').find('select').select('2')
    cy.dataTest('input-deck-size').find('select').select('32')
    cy.dataTest('input-radio-shuffle').click()

    cy.dataTest('room-summary').contains('You\'re going to create a deck of 64 which is not shuffled')
    
    cy.dataTest('form-create-room').submit()
  })
})