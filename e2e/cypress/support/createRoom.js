export const actionName = 'createRoom'

export const action = () => {
  const requestBody = {
    name: 'My room for testing',
    deck: {
      shuffled: true,
      type: 52,
      nbDeck: 1
    }
  }
  cy.request('POST', '/api/rooms', requestBody).then(response => {
    return response.body
  })
}