export const actionName = 'dataTest'

export const action = dataTestAttribute =>
  cy.get(`[data-test="${dataTestAttribute}"]`)