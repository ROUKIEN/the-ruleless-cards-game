import * as createRoom from './createRoom'
import * as dataTest from './dataTest'

Cypress.Commands.add(createRoom.actionName, createRoom.action)
Cypress.Commands.add(dataTest.actionName, dataTest.action)