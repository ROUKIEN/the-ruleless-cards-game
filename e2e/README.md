# End to End testing

This folder contains everything needed to run e2e tests on TRCG.

## Running tests locally

> This assumes that you're running TRCG locally with `make dev` from the root folder.

The tests are launched using the Makefile which is in this folder.

When you're developing a new feature and you want to add some e2e tests, run the test runner locally : 

  - `make test-runner`

It will open the cypress test runner window where you can manually run the specs from there.

Running the tests in CLI :

 - `make test-e2e`
