const PROXY_SERVER_HOSTNAME = process.env.PROXY_SERVER_HOSTNAME || 'localhost'
const SHOW_WEBPACK_PROGRESS = process.env.SHOW_WEBPACK_PROGRESS ? process.env.SHOW_WEBPACK_PROGRESS === 'true' ? true : false : true

module.exports = {
  devServer: {
    progress: SHOW_WEBPACK_PROGRESS,
    proxy: {
      '^/api': {
        target: `http://${PROXY_SERVER_HOSTNAME}:3000`,
        changeOrigin: true
      },
      '/socket.io': {
        target: `http://${PROXY_SERVER_HOSTNAME}:3000`,
        changeOrigin: true,
        ws: true
      }
    }
  }
}
