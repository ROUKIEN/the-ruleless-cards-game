import { toLocal, fromLocal, relativeTo } from '../../../src/components/game/scenes/rotationUtils'
import slotRotations from '../../../src/components/game/scenes/slotRotations'

const localPositions = [
  ['bottom', { x: 1, y: 1 }],
  ['top', { x: -1, y: -1 }],
  ['left', { x: 1, y: -1 }],
  ['right', { x: -1, y: 1 }]
]

const remotePositions = [
  ['bottom', { x: 1, y: 1 }],
  ['top', { x: -1, y: -1 }],
  ['left', { x: 1, y: -1 }],
  ['right', { x: -1, y: 1 }]
]

const remotePosition = {
  x: 1,
  y: 1
}

const center = {
  x: 0,
  y: 0
}

describe('rotationUtils', () => {
  describe('toLocal', () => {
    test.each(localPositions)
    ('Computes the "%s" position according to the local rotation', (rotationContext, expectedLocalPosition) => {
      const localPosition = toLocal(remotePosition, center, slotRotations[rotationContext])

      const precision = 3
      expect(localPosition).toHaveProperty('x')
      expect(localPosition.x).toBeCloseTo(expectedLocalPosition.x, precision)
      expect(localPosition).toHaveProperty('y')
      expect(localPosition.y).toBeCloseTo(expectedLocalPosition.y, precision)
    })
  })

  describe('fromLocal', () => {
    test.each(remotePositions)
    ('Computes the remote position based on the local "%s" position & rotation', (rotationContext, localPosition) => {

      const outputRemotePosition = fromLocal(localPosition, center, slotRotations[rotationContext])

      const precision = 5
      expect(outputRemotePosition).toHaveProperty('x')
      expect(outputRemotePosition.x).toBeCloseTo(remotePosition.x, precision)
      expect(outputRemotePosition).toHaveProperty('y')
      expect(outputRemotePosition.y).toBeCloseTo(remotePosition.y, precision)
    })
  })
})
