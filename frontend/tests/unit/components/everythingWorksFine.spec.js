/*
  BEST. TESTS. EVER.
*/

describe('everything', () => {
  test('works as expected', () => {
    const itWorks = true
    expect(itWorks).toBe(true)
  })
  test('is under control', () => {
    const underControl = true
    expect(underControl).not.toBe(false)
  })
})

describe('everybody', () => {
  test('stays the fucking home', () => {
    const people = [
      { name: 'Dan', isAtHome: true },
      { name: 'Alice', isAtHome: true },
      { name: 'Bob', isAtHome: true },
      { name: 'Michelle', isAtHome: true },
    ]

    people.forEach(aPeople => {
      expect(aPeople).toHaveProperty('isAtHome', true)
    })
  })
})
