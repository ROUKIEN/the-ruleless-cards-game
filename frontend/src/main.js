import Vue from 'vue'
import Buefy from 'buefy'
import 'buefy/dist/buefy.css'

import * as Sentry from '@sentry/browser'
import * as Integrations from '@sentry/integrations'

import App from './App.vue'
import router from './router'
import store from './store'
import i18n from './translations/i18n'

import TestDirective from './directives/TestDirective.js'

Vue.directive('test', TestDirective)

if (process.env.VUE_APP_SENTRY_DSN) {
  Sentry.init({
    dsn: process.env.VUE_APP_SENTRY_DSN,
    environment: process.env.VUE_APP_SENTRY_ENV_NAME || 'undefined',
    integrations: [new Integrations.Vue({ Vue, attachProps: true })]
  })
}

Vue.config.productionTip = false

Vue.use(Buefy)

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
