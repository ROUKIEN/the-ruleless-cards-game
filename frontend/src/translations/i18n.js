import Vue from 'vue'
import VueI18n from 'vue-i18n'

import translations from './index'

Vue.use(VueI18n)

const browserLocale = navigator.language ? navigator.language.substring(0, 2) : 'en'
const locale = browserLocale in translations ? browserLocale : 'en'

export default new VueI18n({
  locale,
  messages: translations
})
