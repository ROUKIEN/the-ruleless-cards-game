const slotMapping = {
  bottom: {
    bottom: 'bottom',
    top: 'top',
    left: 'left',
    right: 'right'
  },
  top: {
    bottom: 'top',
    top: 'bottom',
    left: 'right',
    right: 'left'
  },
  left: {
    bottom: 'right',
    top: 'left',
    left: 'bottom',
    right: 'top'
  },
  right: {
    bottom: 'left',
    top: 'right',
    left: 'top',
    right: 'bottom'
  }
}

/**
 * I am that bad in maths that I had to write a function that converts a slot position into the local position
 *
 * @param {String} slotFrom
 * @param {String} slot
 */
const positionRelativeTo = (slotFrom, slot) => {
  return slotFrom in slotMapping && slot in slotMapping[slotFrom] ? slotMapping[slotFrom][slot] : null
}

export default positionRelativeTo
