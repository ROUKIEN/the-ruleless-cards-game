// angle in radians
const slotRotations = {
  bottom: 2 * Math.PI,
  top: Math.PI,
  right: Math.PI / 2,
  left: Math.PI + Math.PI / 2
}

export default slotRotations
