import { Scene, Input } from 'phaser'
import { throttle } from 'lodash-es'

import cardsAtlas from './assets/cards/cards_atlas.png'
import cardsAtlasJson from './assets/cards/cards_atlas.json'

import slotRotations from './slotRotations'
import { fromLocal, toLocal } from './rotationUtils'
import positionRelativeTo from './positionRelativeTo.js'
import onGameObjectDrag from '../behaviors/onGameObjectDrag.js'
import onGameObjectDragEnd from '../behaviors/onGameObjectDragEnd.js'

import i18n from '../../../translations/i18n'

const colors = [
  'spades',
  'diamonds',
  'clubs',
  'hearts'
]

const slotOffset = 350
const slotPositions = {
  bottom: {
    x: 0,
    y: slotOffset
  },
  top: {
    x: 0,
    y: -slotOffset
  },
  left: {
    x: -slotOffset,
    y: 0
  },
  right: {
    x: slotOffset,
    y: 0
  }
}

const center = { x: 400, y: 400 }
// relative to the cards container
const tableCenter = { x: 0, y: 0 }

export default class GameScene extends Scene {
  constructor () {
    super('game-scene')
    this.cardsContainer = null
    this.socket = null
    this.room = null
    this.tableContainer = null
    this.playerHandContainer = null
    this.handZone = null
    this.cardsInHand = []
    this.cardsCounter = {}
    this.pointersContainer = null
    this.players = {}
    this.hooks = {}
    this.user = null
    this.slot = null
    this.size = {
      x: 800,
      y: 800
    }
    this.throttledOnPointerMove = throttle(this.onPointerMove, 150)
  }

  init (data) {
    this.socket = data.socket
    this.room = data.room
    this.user = data.user
    this.hooks = data.hooks
  }

  preload () {
    this.load.atlas('cards', cardsAtlas, cardsAtlasJson)

    this.cardsContainer = this.add.container(0, 0)
    this.pointersContainer = this.add.container(-this.size.x / 2, -this.size.y / 2)
    this.playerHandContainer = this.add.container(0, 900)
    this.tableContainer = this.add.container(this.size.x / 2, this.size.y / 2)

    this.tableContainer.add(this.cardsContainer)
    this.tableContainer.add(this.pointersContainer)

    this.handZone = this.add.rectangle(400, 0, 800, 200, 0x44BB44)
    this.handZone.setInteractive({ dropZone: true })
    this.playerHandContainer.add(this.handZone)

    const line = this.add.line(300, -100, 100, 0, 700, 0, 0xFFFFFF)
    line.setLineWidth(3, 3)
    this.playerHandContainer.add(line)

    const handText = this.add.text(750, 90, i18n.t('game.hand.label'), { fontSize: 32, color: 'white' })
    handText.setAngle(-90)
    this.playerHandContainer.add(handText)
  }

  create () {
    this.socket.emit('user:define', { user: this.user })

    this.socket.on('table:reset:success', payload => this.hooks.onTableReset(payload))

    this.socket.on('pointer:moved', payload => {
      if (this.user.id === payload.user.id) {
        return
      }

      const pointerToMove = this.players[payload.user.id].pointer
      const localPos = toLocal(payload.pointer, center, slotRotations[this.slot.name])
      pointerToMove.x = localPos.x
      pointerToMove.y = localPos.y
    })

    this.input.on(Input.Events.DROP, (pointer, cardSprite, zone) => {
      const card = cardSprite.parentContainer

      const alreadyInPlayerHand = card.playerHand === this.user.id
      if (alreadyInPlayerHand) {
        return
      }

      card.addToPlayerHand(this.user.id)
      this.cardsInHand.push(card)
      card.setRotation(0)
      card.y = 400 + 100 + this.handZone.y // the cards container height / 2 + the hand container height / 2

      !card.isFaceUp && card.flip()

      const thePlayer = this.players[this.user.id]
      this.players[this.user.id] = this.updatePlayerCardsCount(thePlayer, thePlayer.cardsCount + 1)
      this.socket.emit('player:card:draw', {
        cardId: card.id,
        userId: this.user.id,
        roomId: this.room.id
      })
    })

    this.input.on(Input.Events.DRAG_END, onGameObjectDragEnd, this)
    this.input.on(Input.Events.DRAG, onGameObjectDrag, this)

    this.input.on(Input.Events.DRAG_LEAVE, (pointer, cardSprite, zone) => {
      const card = cardSprite.parentContainer
      card.releaseFromHand()

      const cardIsInHand = this.cardsInHand.find(aHandCard => aHandCard.name === card.name)
      if (!cardIsInHand) {
        return
      }

      card.isFaceUp === true && card.flip()
      // Remove the card from the player's hand
      this.cardsInHand = this.cardsInHand.filter(handCard => handCard.name !== card.name)
      const thePlayer = this.players[this.user.id]
      this.players[this.user.id] = this.updatePlayerCardsCount(thePlayer, thePlayer.cardsCount - 1)
      this.socket.emit('player:card:play', {
        cardId: card.id,
        userId: this.user.id,
        roomId: this.room.id,
        rotation: card.rotation - slotRotations[this.slot.name]
      })
    })

    this.socket.on('user:define:success', () => {
      this.hooks.onRoomJoining()
      this.socket.emit('room:join', { roomId: this.room.id })
    })

    this.socket.on('room:join:fail', payload => { this.hooks.onJoinRoomError(payload) })

    this.socket.on('player:flush-hand', ({ cards, coords, rotation }) => {
      cards.forEach(card => {
        const localCardCoords = toLocal(coords, tableCenter, slotRotations[this.slot.name])
        const atlasKey = `card_${colors[card.card.color - 1]}_${card.card.value}`
        card.frameName = atlasKey
        const theCard = this.add.card(localCardCoords.x, localCardCoords.y, card)
        this.bindCardEvents(theCard)
        theCard.setRotation(slotRotations[this.slot.name] + rotation)
        theCard.setRotationEnabled(true)
        this.cardsContainer.add(theCard)
      })
    })

    this.socket.on('player:card:draw:success', ({ cardId, userId, cardsCount }) => {
      this.players[userId] = this.updatePlayerCardsCount(this.players[userId], cardsCount)

      const cardContainer = this.cardsContainer.list.find(tableCard => tableCard.id === cardId)
      if (!cardContainer) {
        console.warn(`card ${cardId} was not found.`)
        return
      }
      this.cardsContainer.remove(cardContainer, true)
    })

    this.socket.on('player:card:play:success', ({ card, userId, cardsCount }) => {
      this.players[userId] = this.updatePlayerCardsCount(this.players[userId], cardsCount)

      const atlasKey = `card_${colors[card.card.color - 1]}_${card.card.value}`
      card.frameName = atlasKey
      const theCardContainer = this.add.card(card.position.x, card.position.y, card)
      this.bindCardEvents(theCardContainer)
      this.cardsContainer.add(theCardContainer)
    })

    this.socket.on('table:status', ({ table, players }) => {
      players.forEach(player => {
        if (player.id in this.players) {
          this.players[player.id].playerLabel.setText(`${player.username} (${player.cardsCount})`)
          this.players[player.id].cardsCount = player.cardsCount
        } else {
          const pointer = this.add.playerPointer(player)
          const playerLabelPosition = slotPositions[positionRelativeTo(this.slot.name, player.slot)]
          const playerLabel = this.createPlayerLabel(player, playerLabelPosition)
          this.players[player.id] = {
            id: player.id,
            username: player.username,
            cardsCount: player.cardsCount,
            pointer,
            playerLabel
          }
        }
      })

      this.cardsContainer.removeAll()

      table.cards
        .filter(card => card.visible === true)
        .map(({ id, card, coords, rotation, isFaceUp }) => {
          const localCoords = toLocal(coords, tableCenter, slotRotations[this.slot.name])
          const frameName = `card_${colors[card.color - 1]}_${card.value}`
          const theCard = this.add.card(localCoords.x, localCoords.y, { id, frameName, isFaceUp })

          this.bindCardEvents(theCard)

          this.cardsContainer.addAt(theCard, card.position) // bug ?
          theCard.setRotationEnabled(true)

          theCard.setRotation(slotRotations[this.slot.name] + rotation)
        })
    })

    this.socket.on('card:move', ({ card }) => {
      const theCard = this.cardsContainer.list.find(currentCard => currentCard.id === card.id)
      const localCoords = toLocal(card.coords, tableCenter, slotRotations[this.slot.name])
      theCard.x = localCoords.x
      theCard.y = localCoords.y
      theCard.setRotation(slotRotations[this.slot.name] + card.rotation)
      this.cardsContainer.bringToTop(theCard)
    })

    this.socket.on('card:flipped', ({ cardId }) => {
      const theCard = this.cardsContainer.list.find(currentCard => currentCard.id === cardId)
      theCard.flip()
    })

    this.socket.on('room:players:update', payload => {
      this.hooks.onPlayerUpdate(payload)
      if ('entered' in payload && this.user.id !== payload.entered.id) {
        const pointer = this.add.playerPointer(payload.entered)
        this.pointersContainer.add(pointer)
        const playerLabelPosition = slotPositions[positionRelativeTo(this.slot.name, payload.slot)]
        const playerLabel = this.createPlayerLabel(payload.entered, playerLabelPosition)

        this.players[payload.entered.id] = {
          id: payload.entered.id,
          username: payload.entered.username,
          cardsCount: 0,
          pointer,
          playerLabel
        }
      } else if ('leaved' in payload) {
        const theLeavingPlayer = this.players[payload.leaved.id]
        this.pointersContainer.remove(theLeavingPlayer.pointer, true)
        theLeavingPlayer.playerLabel.destroy()
        theLeavingPlayer.pointer.destroy()

        delete this.players[payload.leaved.id]
      }
    })

    this.socket.on('room:joined', ({ players, slot }) => {
      this.hooks.onRoomJoined(players)
      this.slot = slot

      this.input.on(Input.Events.POINTER_MOVE, this.throttledOnPointerMove, this)
    })

    this.socket.on('player:update:success', ({ user }) => {
      if (user.id in this.players === false) {
        return
      }

      this.players[user.id].username = user.username
      this.players[user.id].playerLabel.setText(`${user.username} (${this.players[user.id].cardsCount})`)
      this.players[user.id].pointer.rename(user.username)
      this.players[user.id].pointer.setColor(user.color)
    })

    this.socket.on('room:unknown', this.hooks.onRoomUnknown)
  }

  updatePlayerCardsCount (player, cardsCount) {
    console.log(`Updating "${player.username}" hand cards count to ${cardsCount}`)
    player.cardsCount = cardsCount
    player.playerLabel.setText(`${player.username} (${cardsCount})`)
    return player
  }

  createPlayerLabel (user, at) {
    const playerLabel = this.add.text(at.x, at.y, `${user.username} (${user.cardsCount})`, { color: 'white' })
    this.tableContainer.add(playerLabel)
    playerLabel.setName(`label-${user.id}`)

    return playerLabel
  }

  onPointerMove (pointer) {
    const normalizedPointer = fromLocal(pointer.position, center, slotRotations[this.slot.name])

    this.socket.emit('pointer:move', {
      pointer: normalizedPointer,
      room: this.room,
      user: {
        id: this.user.id
      }
    })
  }

  bindCardEvents (card) {
    card.on('flip', ({ card }) => {
      this.socket.emit('card:flip', {
        room: this.room,
        cardId: card.id
      })
    })

    card.on('rotate', ({ card }) => {
      this.cardsContainer.bringToTop(card)
      this.socket.emit('card:rotate', {
        card: {
          id: card.id,
          rotation: card.rotation - slotRotations[this.slot.name]
        },
        roomId: this.room.id
      })
    })

    card.on('move', ({ card }) => {
      this.cardsContainer.bringToTop(card)
      // compute coords for remote with `fromLocal`
      const coords = fromLocal(card, tableCenter, slotRotations[this.slot.name])
      this.socket.emit('card:drag', {
        card: {
          id: card.id,
          coords,
          rotation: card.rotation - slotRotations[this.slot.name]
        },
        roomId: this.room.id
      })
    })
  }
}
