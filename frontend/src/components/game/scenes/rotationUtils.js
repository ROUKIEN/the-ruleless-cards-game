// import slotRotations from './slotRotations'

/**
 * @param {Coord} remotePosition the position to convert in the current context
 * @param {Coord} center the center used to rotate the remotePosition
 * @param {Number} angle in radians. The current context angle
 *
 * @returns {Coord} the local position
 */
const toLocal = (remotePosition, center, angle) => {
  return {
    x: (Math.cos(angle) * (remotePosition.x - center.x) - Math.sin(angle) * (remotePosition.y - center.y)) + center.x,
    y: (Math.sin(angle) * (remotePosition.x - center.x) + Math.cos(angle) * (remotePosition.y - center.y)) + center.y
  }
}

/**
 * @param {Coord} localPosition the position to convert in the remote context
 * @param {Coord} center the center used to rotate the local position
 * @param {Number} angle in radians. The current context angle
 *
 * @returns {Coord} the remote position
 */
const fromLocal = (localPosition, center, angle) => {
  const negativeAngle = -angle
  return {
    x: (Math.cos(negativeAngle) * (localPosition.x - center.x) - Math.sin(negativeAngle) * (localPosition.y - center.y)) + center.x,
    y: (Math.sin(negativeAngle) * (localPosition.x - center.x) + Math.cos(negativeAngle) * (localPosition.y - center.y)) + center.y
  }
}

/**
 * @typedef Coord a 2D coords object
 *
 * @property {Number} x
 * @property {Number} y
 */

module.exports = {
  toLocal,
  fromLocal
}
