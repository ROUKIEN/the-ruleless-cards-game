import { GameObjects, Input } from 'phaser'
import proceedToCardFlip from '../behaviors/proceedToCardFlip.js'

const CARD_FACE_DOWN = 'back'

class Card extends GameObjects.Container {
  constructor (scene, x, y, card) {
    super(scene, x, y)

    this.isFaceUp = card.isFaceUp
    this.color = 1
    this.value = 1
    this.frameName = card.frameName

    this.flippable = true
    this.initialAngle = null

    this.id = card.id
    this.name = card.frameName
    this.proceedToCardFlip = proceedToCardFlip(this)
    this.lastTimeClicked = 0

    this.playerHand = null

    this.cardSprite = this.scene.add.image(0, 0, 'cards', card.isFaceUp ? card.frameName : CARD_FACE_DOWN)
    this.cardSprite.setName(`card-${this.id}`)
    this.cardSprite.setInteractive({ cursor: 'move' })

    this.add(this.cardSprite)

    const rotationZones = [
      {
        x: -this.cardSprite.width / 3,
        y: -this.cardSprite.height / 3
      },
      {
        x: this.cardSprite.width / 3,
        y: -this.cardSprite.height / 3
      },
      {
        x: -this.cardSprite.width / 3,
        y: this.cardSprite.height / 3
      },
      {
        x: this.cardSprite.width / 3,
        y: this.cardSprite.height / 3
      }
    ]

    this.rotationAreas = rotationZones.map((rotationZone, index) => {
      const rectangle = this.scene.add.rectangle(rotationZone.x, rotationZone.y, 50, 50, 0x00FF00, 0)
        .setName(`rotation-${index}`)
        .setInteractive({ cursor: 'grab' })
      this.add(rectangle)

      return rectangle
    })

    this.bindEvents()
  }

  flip () {
    this.isFaceUp = !this.isFaceUp
    const newFrame = this.isFaceUp ? this.frameName : CARD_FACE_DOWN
    this.cardSprite.setFrame(newFrame)
  }

  setRotationEnabled (yesOrNo) {
    this.scene.input.setDraggable(this.cardSprite, yesOrNo)
    this.rotationAreas.forEach(rotationArea => this.scene.input.setDraggable(rotationArea, yesOrNo))
  }

  addToPlayerHand (playerId) {
    this.playerHand = playerId
  }

  releaseFromHand () {
    this.playerHand = null
  }

  /**
   * @private
   */
  bindEvents () {
    this.rotationAreas.forEach(rotationArea => rotationArea.on(Input.Events.POINTER_DOWN, this.proceedToCardFlip))
    this.cardSprite.on(Input.Events.POINTER_DOWN, this.proceedToCardFlip)
  }
}

export default Card
