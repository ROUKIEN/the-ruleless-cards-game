import { Plugins } from 'phaser'
import Card from './Card'
import PlayerPointer from './PlayerPointer'

class TrcgPlugin extends Plugins.BasePlugin {
  constructor (pluginManager) {
    super(pluginManager)

    pluginManager.registerGameObject('card', this.createCard)
    pluginManager.registerGameObject('playerPointer', this.createPlayerPointer)
  }

  createCard (x, y, card) {
    return this.displayList.add(new Card(this.scene, x, y, card))
  }

  createPlayerPointer (player) {
    return this.displayList.add(new PlayerPointer(this.scene, -100, -100, player))
  }
}

export default TrcgPlugin
