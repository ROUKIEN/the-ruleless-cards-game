import { GameObjects } from 'phaser'

const color2Hex = color =>
  parseInt(color.replace(/^#/, ''), 16)

/**
 * Each other player is affected a PlayerPointer ;
 * It allows a player to know where other players mice are located in the game window.
 */
class PlayerPointer extends GameObjects.Container {
  constructor (scene, x, y, player) {
    super(scene, x, y)

    this.color = player.color
    this.colorAsHex = color2Hex(this.color)
    this.playerName = player.username

    this.pointer = this.scene.add.circle(0, 0, 5, this.colorAsHex)
    this.pointer.setStrokeStyle(2, 0xFFFFFF)
    this.playerLabel = this.scene.add.text(10, 0, this.playerName, { fontSize: 12, color: this.color })

    this.setName(player.id)

    this.add(this.pointer)
    this.add(this.playerLabel)
  }

  rename (newName) {
    this.playerName = newName

    this.playerLabel.setText(newName)
  }

  setColor (newColor) {
    this.color = newColor
    this.colorAsHex = color2Hex(newColor)

    this.pointer.setFillStyle(this.colorAsHex)
    this.playerLabel.setColor(newColor)
  }
}

export default PlayerPointer
