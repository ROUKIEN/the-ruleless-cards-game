/**
 * Creates a function that will flip the given card when called.
 *
 * @param {Object} card
 */
const proceedToCardFlip = card =>
  () => {
    if (!card.flippable) {
      return
    }

    const clickDelay = card.scene.time.now - card.lastTimeClicked
    card.lastTimeClicked = card.scene.time.now
    if (clickDelay < 350) {
      card.flip()
      card.emit('flip', { card })
      // it prevents from flipping a card too quickly after a first flip
      card.lastTimeClicked = 1000
    }
  }

export default proceedToCardFlip
