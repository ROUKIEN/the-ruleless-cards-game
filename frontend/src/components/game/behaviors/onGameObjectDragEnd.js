const onGameObjectDragEnd = (pointer, gameObject) => {
  const card = gameObject.parentContainer
  card.initialAngle = null
}

export default onGameObjectDragEnd
