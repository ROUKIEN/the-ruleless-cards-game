import Phaser from 'phaser'

const center = { x: 400, y: 400 }

const onGameObjectDrag = (pointer, object) => {
  const card = object.parentContainer

  if (object === card.cardSprite) {
    card.x = -center.x + pointer.x

    if (card.playerHand === null) {
      card.y = -center.y + pointer.y
      card.emit('move', { card })
    }
  } else if (card.rotationAreas.find(rotationArea => object === rotationArea)) {
    if (card.playerHand !== null) {
      return
    }

    const mouse = {
      x: -center.x + pointer.x,
      y: -center.y + pointer.y
    }

    if (card.initialAngle === null) {
      card.initialAngle = Phaser.Math.Angle.BetweenPoints(card, mouse) - card.rotation
    }

    const currentAngle = Phaser.Math.Angle.BetweenPoints(card, mouse)
    const newAngle = Phaser.Math.Angle.ShortestBetween(card.initialAngle, currentAngle)

    card.setRotation(newAngle)

    card.emit('rotate', { card })
  }
}

export default onGameObjectDrag
