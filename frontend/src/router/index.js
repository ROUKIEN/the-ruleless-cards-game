import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'
import { SnackbarProgrammatic as Snackbar } from 'buefy'
import store from '../store'

import i18n from '../translations/i18n'

import Homepage from '../views/Homepage.vue'
import JoinRoom from '../views/JoinRoom.vue'
import CreateRoom from '../views/CreateRoom.vue'
import Room from '../views/Room.vue'
import Oops from '../views/Oops.vue'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'homepage',
      component: Homepage,
      meta: {
        title: 'Home'
      }
    },
    {
      path: '/join',
      name: 'join-room',
      component: JoinRoom,
      meta: {
        title: i18n.tc('room.join.title')
      }
    },
    {
      path: '/new',
      name: 'create-room',
      component: CreateRoom,
      meta: {
        title: i18n.tc('room.create')
      }
    },
    {
      path: '/oops',
      name: 'oops',
      component: Oops,
      props: true,
      meta: {
        title: 'Whoops !'
      }
    },
    {
      path: '/room/:roomId',
      name: 'room',
      component: Room,
      props: true,
      beforeEnter: async (routeTo, routeFrom, next) => {
        const { roomId } = routeTo.params
        try {
          const response = await axios.get(`${process.env.VUE_APP_API_ENDPOINT}/rooms/${roomId}`)
          routeTo.params.room = response.data
          next()
        } catch (error) {
          if (error.response.status === 404) {
            Snackbar.open({
              message: i18n.tc('room.not-found', { roomId }),
              position: 'is-top',
              type: 'is-warning'
            })
            next({ name: 'homepage' })
          } else {
            next({
              name: 'oops',
              params: {
                code: error.response.status,
                message: i18n.t('room.fetch-failed')
              }
            })
            console.error(error)
          }
        }
      },
      meta: {
        title: i18n.tc('room.meta')
      }
    }
  ]
})

router.beforeEach((to, from, next) => {
  store.dispatch('loadUser')
  next()
})

export default router
