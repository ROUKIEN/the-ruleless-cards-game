import Vue from 'vue'
import Vuex from 'vuex'

import User from './modules/User'

Vue.use(Vuex)

const strict = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    User
  },
  strict
})
