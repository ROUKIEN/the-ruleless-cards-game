import { v4 } from 'uuid'
import { SET_USER } from './mutations-types'
import i18n from '../../../translations/i18n'

function randomUsernameSuffix () {
  let suffix = ''
  let i = 5
  do {
    suffix += '0123456789abcdef'.substr(Math.random() * 16, 1)
  } while (i--)

  return suffix
}

/**
 * Retrieve the user informations from LocalStorage
 */
export const loadUser = ({ commit, dispatch, state }) => {
  if (state.id !== '') {
    return
  }

  let user = JSON.parse(localStorage.getItem('user'))
  if (user === null) {
    user = {
      username: `${i18n.t('labels.player')}-${randomUsernameSuffix()}`,
      color: `#${randomUsernameSuffix()}`,
      id: v4()
    }

    dispatch('saveUser', user)
    return
  }

  commit(SET_USER, user)
}

/**
 * Saves the user informations into LocalStorage
 */
export const saveUser = ({ commit }, user) => {
  localStorage.setItem('user', JSON.stringify(user))

  commit(SET_USER, user)
}

/**
 * Updates the user's username
 */
export const updateUsername = ({ dispatch }, username) => {
  const user = JSON.parse(localStorage.getItem('user'))
  user.username = username

  dispatch('saveUser', user)
}

/**
 * Updates the user's color
 */
export const updateColor = ({ dispatch }, color) => {
  const user = JSON.parse(localStorage.getItem('user'))

  user.color = color

  dispatch('saveUser', user)
}
