import state from './state.js'
import mutations from './mutations.js'
import * as actions from './actions.js'

export default {
  state,
  mutations,
  actions
}
