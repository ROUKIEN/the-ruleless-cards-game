import { SET_USER } from './mutations-types'

export default {
  [SET_USER]: (state, user) => {
    state.username = user.username
    state.id = user.id
    state.color = user.color
  }
}
