export default {
  bind (el, binding) {
    el.setAttribute('data-test', binding.arg)
  }
}
