# The ruleless cards game 

[![pipeline status](https://gitlab.com/ROUKIEN/the-ruleless-cards-game/badges/master/pipeline.svg)](https://gitlab.com/ROUKIEN/the-ruleless-cards-game/-/commits/master)

Online cards game are great, but they miss something: whinning about the game's rules.

# About TRCG

This app is basically a card game sandbox. Set up a room, pickup a card deck, invite some friends & just play cards to the game you want ! No rules are defined !

# Creating a room

Just give it a name. Everybody is allowed to join your room as a spectator, they need your approval to be able to join the table.

# Joining the table

Time to play ! Just listen to your table host about the rules & start playing. Don't forget that even if you're able to do anything means that you're allowed to ! It's like card games in real life !

# About the project

## Structure

The app is composed of a server (nodejs, express, socket.io) and a ui (vue & phaser).

# Contributing

## Prerequisites

- docker & docker-compose
- make

## Setting up

Start by cloning the repo, then :

1. `make configure`
2. `make install`
3. `make dev`

The ui is reachable through port `:8080` and the server through port `:3000`

When you're performing code updates, the app will automagically restart.

## Testing 🚦

1. `make test-backend` (`make test-backend-watch` for relaunching tests at every code update)
1. `make test-frontend` (`make test-frontend-watch` for relaunching tests at every code update)

## Stop all

`make stop`

## Remove everything

`make clean`

If it's not sufficient, run `make mrproper`.

## Scale things

If you need to run more than one trcg server, you'll need some extra stuff:

1. spin up a redis server
2. set the `ADAPTER_NAME` env variable to 'redis'
3. set the `ADAPTER_REDIS_HOST` to the redis hostname (default: `localhost`)
3. set the `ADAPTER_REDIS_PORT` to the redis port (default: `6379`)

Then you can start the app.

## Error tracking

### Server

You can define a Sentry instance using some environment variables for the server.

1. set the `SENTRY_DSN` env var with the sentry dsn you want
2. set the `SENTRY_ENV_NAME` env var to set an environment name that sentry will use

### UI

You can define a Sentry instance using some environment variables for the ui too.

1. set the `SENTRY_DSN` env var with the sentry dsn you want
2. set the `SENTRY_ENV_NAME` env var to set an environment name that sentry will use

When processing to build, those environment variables will be set in a `frontend/.env.local` that will be used during the app build.

## Staging version

The staging is deployed on Heroku at [https://trcg-staging.herokuapp.com](https://trcg-staging.herokuapp.com)