const { Card, Deck } = require('../src/model')
const colors = require('../src/model/cards/colors')
const values = require('../src/model/cards/values')
const shuffle = require('../src/rooms/shuffle')

const exclusionList = {
  52: [],
  32: [2, 3, 4, 5, 6]
}

const deck = options => {
  const cards = []
  for (let i = 0; i < options.nbDeck; i++) {
    Object.keys(colors).forEach(colorKey => {
      Object.keys(values).forEach(valueKey => {
        if (!exclusionList[options.type].includes(parseInt(valueKey))) {
          cards.push(new Card(colorKey, valueKey))
        }
      })
    })
  }
  options.shuffled && shuffle(cards)

  return new Deck(cards)
}

module.exports = deck
