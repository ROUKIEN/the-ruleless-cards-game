const { Table, TableCard } = require('../../../src/rooms/table')

describe('Table', () => {
  describe('slots', () => {
    test('have 4 default slots to top, bottom, left, right', () => {
      const table = new Table()
      expect(table).toHaveProperty('slots')
      expect(table.slots).toHaveLength(4)
    })

    test('are all available by default', () => {
      const table = new Table()
      table.slots.forEach(slot => {
        expect(slot.isAvailable()).toBe(true)
      })
    })

    test('affects a slot to a joining player', () => {
      const table = new Table()
      const userId = 'qzeoifjzef'

      table.join(userId)

      const firstSlot = table.slots[0]
      expect(firstSlot.getUserId()).toBe('qzeoifjzef')
      expect(firstSlot.isAvailable()).toBe(false)
    })

    test('releases the user slot when a user leaves', () => {
      const table = new Table()
      const userIds = ['qzeoifjzef', 'zogiehrgijerg', 'fzoifzefojiz']
      userIds.map(userId => table.join(userId))

      table.leave('zogiehrgijerg')

      const secondSlot = table.slots[1]
      expect(secondSlot.isAvailable()).toBe(true)
      expect(secondSlot.getUserId()).toBe(null)
    })

    test('throws an error when a user joins a table with no available slots', () => {
      const table = new Table()
      const userIds = ['qzeoifjzef', 'zogiehrgijerg', 'fzoifzefojiz', 'zefizjfeoizerhu']
      userIds.map(userId => table.join(userId))

      const theFifthUserJoining = () => {
        table.join('thefifthuserid')
      }

      expect(theFifthUserJoining).toThrow('Table does not contain any available slots.')
    })

    test('assigns the first available slot to the joining player', () => {
      const table = new Table()
      const theThirdPlayer = 'fzoifzefojiz'
      const userIds = ['qzeoifjzef', 'zogiehrgijerg', theThirdPlayer, 'zefizjfeoizerhu']
      userIds.map(userId => table.join(userId))

      table.leave(theThirdPlayer)

      table.join('thefifthuserid')

      const thirdSlot = table.slots[2]
      expect(thirdSlot.isAvailable()).toBe(false)
      expect(thirdSlot.getUserId()).toBe('thefifthuserid')
    })
  })

  describe('cards', () => {
    test('are empty by default', () => {
      const table = new Table()

      expect(table.cards).toHaveLength(0)
      expect(table.maxPosition).toBe(0)
    })

    test('maxPosition is updated according to the cards number', () => {
      const cards = [
        new TableCard({ color: 11, value: 11 }, { x: 0, y: 0 }, 0),
        new TableCard({ color: 11, value: 11 }, { x: 0, y: 0 }, 0)
      ]
      const table = new Table(cards)

      expect(table.cards).toHaveLength(2)
      expect(table.maxPosition).toEqual(1)
    })
  })
})
