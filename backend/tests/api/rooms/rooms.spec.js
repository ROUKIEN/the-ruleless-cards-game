const request = require('supertest')
const { v4 } = require('uuid')
const { createAPI } = require('../../../src/api')
const db = require('../../../src/db/db')

const api = createAPI(false)

const testDb = process.env.MONGO_URL || 'mongodb://localhost:27017'
const dbName = 'trcg-testing'

beforeAll(() => {
  return db.connect(testDb, dbName)
})

afterAll(() => {
  db.disconnect()
})

describe('Rooms endpoints', () => {
  describe('POST /rooms', () => {
    it('Must create a new room', async () => {
      const res = await request(api)
        .post('/api/rooms')
        .send({
          name: 'My room',
          deck: {
            shuffled: true,
            type: 52,
            nbDeck: 2
          }
        })

      expect(res.statusCode).toEqual(201)

      expect(res.body).toEqual(expect.objectContaining({
        id: expect.any(String),
        name: 'My room',
        private: true,
        deck: expect.objectContaining({
          shuffled: true,
          type: 52,
          nbDeck: 2,
        })
      }))
      expect(res.body).toHaveProperty('createdAt')
    })

    it('returns a 422 when values are invalid', async () => {
      const res = await request(api)
        .post('/api/rooms')
        .send({
          name: 'My room with a deck of 1337 cards, lulz',
          deck: {
            shuffled: true,
            type: 1337,
            nbDeck: 2
          }
        })

      expect(res.statusCode).toEqual(422)

      expect(res.body).toEqual(expect.objectContaining({
        errors: expect.arrayContaining([
          expect.objectContaining({
            location: "body",
            msg: "Invalid value",
            param: "deck.type"
          })
        ])
      })
      )
    })
  })

  describe('GET /api/rooms/:roomId', () => {
    it('Fails on unexisting room', async () => {
      const res = await request(api)
        .get(`/api/rooms/${v4()}`)
        .send()

      expect(res.statusCode).toEqual(404)
      expect(res.body).toHaveProperty('message', 'room not found')
    })
  })
})
