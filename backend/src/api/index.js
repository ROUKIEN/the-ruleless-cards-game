const express = require('express')
const Sentry = require('@sentry/node')
const history = require('connect-history-api-fallback')
const bodyParser = require('body-parser')
const morgan = require('morgan')

function createAPI (enableErrorTracking = false) {
  const api = express()
  enableErrorTracking && api.use(Sentry.Handlers.requestHandler())
  const staticFileMiddleware = express.static('dist')

  const rooms = require('./routes/rooms')

  const jsonParser = bodyParser.json()

  api.use(jsonParser)
  api.use(morgan('combined'))

  api.use('/api/rooms', rooms)

  api.use(staticFileMiddleware)
  api.use(history())
  api.use(staticFileMiddleware)

  enableErrorTracking && api.use(Sentry.Handlers.errorHandler())

  return api
}

module.exports = {
  createAPI
}
