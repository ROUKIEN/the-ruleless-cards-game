const { checkSchema } = require('express-validator')

const allowedDeckTypes = [
  32,
  52
]

const allowedNumberOfDecks = [
  1,
  2
]

const checkRoomSchema = checkSchema({
  name: {
    isString: true,
    isLength: {
      options: { min: 2 }
    }
  },
  "deck.shuffled": {
    isBoolean: true
  },
  "deck.type": {
    isInt: true,
    toInt: true,
    isIn: {
      options: [ allowedDeckTypes ]
    }
  },
  "deck.nbDeck": {
    isInt: true,
    toInt: true,
    isIn: {
      options: [ allowedNumberOfDecks ]
    }
  }
})

module.exports = {
  checkRoomSchema
}
