const { v4 } = require('uuid')
const { validationResult } = require('express-validator')

const { Table, TableCard } = require('../../../rooms/table')
const cards = require('../../../../decks/cards.js')
const { roomRepository } = require('../../../db/repos')
const defaultCardCoords = require('../../../../decks/defaultCardCoords.js')
const logger = require('../../../logger')



const createRoom = ({ name, deck }) => ({
  id: v4(),
  name,
  deck,
  createdAt: new Date(),
  private: true
})

/**
 * Find a table in the app
 * @param {Object} req
 * @param {Object} res
 */
const getRoomAction = (req, res) => {
  roomRepository.findById(req.params.roomId, true)
    .then(result => {
      if (result) {
        res.status(200).json(result)
      } else {
        res.status(404).json({ message: 'room not found' })
      }
    })
}

/**
 * Saves a room in db. Also generates a deck for the room
 * @param {Object} req
 * @param {Object} res
 */
const createRoomAction = (req, res) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({
      errors: errors.array()
    })
  }

  const room = createRoom(req.body)
  const deckCards = cards(room.deck).getCards()

  const initialDeck = deckCards.map((card, position) => new TableCard(card, defaultCardCoords, position))
  logger.debug(`generated deck for room ${room.id}`)

  room.table = new Table(initialDeck)

  roomRepository.save(room)
    .then(result => {
      logger.debug(`Saved room ${room.id}`)

      const theRoom = { ...result.ops[0] }
      delete theRoom._id
      delete theRoom.table

      res.status(201).json(theRoom)
    })
}

module.exports = {
  getRoomAction,
  createRoomAction
}
