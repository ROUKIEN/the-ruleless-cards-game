const { Router } = require('express')

const { checkRoomSchema } = require('./roomConstraints')
const { createRoomAction, getRoomAction } = require('./actions')

const rooms = Router()

/**
 * Retrieve a room by its identifier
 */
rooms.get('/:roomId', getRoomAction)

/**
 * Create a room
 */
rooms.post('/', [checkRoomSchema], createRoomAction)

module.exports = rooms
