const VALUE_ACE = 1
const VALUE_TWO = 2
const VALUE_THREE = 3
const VALUE_FOUR = 4
const VALUE_FIVE = 5
const VALUE_SIX = 6
const VALUE_SEVEN = 7
const VALUE_EIGHT = 8
const VALUE_NINE = 9
const VALUE_TEN = 10
const VALUE_JACK = 11
const VALUE_QUEEN = 12
const VALUE_KING = 13

const values = {
  [VALUE_ACE]: 'Ace',
  [VALUE_TWO]: 'Two',
  [VALUE_THREE]: 'Three',
  [VALUE_FOUR]: 'Four',
  [VALUE_FIVE]: 'Five',
  [VALUE_SIX]: 'Six',
  [VALUE_SEVEN]: 'Seven',
  [VALUE_EIGHT]: 'Eight',
  [VALUE_NINE]: 'Nine',
  [VALUE_TEN]: 'Ten',
  [VALUE_JACK]: 'Jack',
  [VALUE_QUEEN]: 'Queen',
  [VALUE_KING]: 'King',
}

module.exports = values
