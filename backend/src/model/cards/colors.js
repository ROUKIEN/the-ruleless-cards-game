const COLOR_HEARTS = 1
const COLOR_SPADES = 2
const COLOR_CLUBS = 3
const COLOR_DIAMONDS = 4

const colors = {
  [COLOR_HEARTS]: 'hearts',
  [COLOR_SPADES]: 'spades',
  [COLOR_CLUBS]: 'clubs',
  [COLOR_DIAMONDS]: 'diamonds',
}

module.exports = colors
