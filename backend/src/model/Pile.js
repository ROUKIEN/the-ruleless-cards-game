class Pile {
  constructor (cards = []) {
    this.cards = cards
  }

  add (card) {
    this.cards.push(card)
  }

  remove (card) {
    this.cards = this.cards.filter(
      currentCard => currentCard.color != card.color || currentCard.value != card.value
    )
  }

  shuffle () {
    let currentIndex = this.cards.length
    let tmpValue, randomIndex
    while (0 !== currentIndex) {
      randomIndex = Math.floor(Math.random() * currentIndex)
      currentIndex -= 1

      tmpValue = this.cards[currentIndex]
      this.cards[currentIndex] = this.cards[randomIndex]
      this.cards[randomIndex] = tmpValue
    }
  }
}

module.exports = Pile
