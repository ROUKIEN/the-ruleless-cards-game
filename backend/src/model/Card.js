const colors = require('./cards/colors.js')
const values = require('./cards/values.js')

class Card {
  constructor (color, value) {
    this.color = color
    this.value = value
  }

  toString () {
    return `${values[this.value]} of ${colors[this.color]}`
  }
}

module.exports = Card
