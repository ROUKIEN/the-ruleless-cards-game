class Hand {
  constructor () {
    this.cards = []
  }
  /**
   * Draw one card from a deck.
   * The card will be removed from the deck and added to the hand.
   */
  draw (deck) {
    
  }

  /**
   * Plays a card from the hand.
   * The card is not in the hand anymore.
   */
  play (card) {

  }
}

module.exports = Hand
