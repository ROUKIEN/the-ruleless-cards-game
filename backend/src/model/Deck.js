/**
 * A current party cards collection.
 * This represents absolutely all the cards that are in game.
 */
class Deck {
  constructor (cards) {
    this.cards = cards
  }

  getCards () {
    return this.cards
  }
}

module.exports = Deck
