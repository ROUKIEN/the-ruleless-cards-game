function formatPlayerForFront (clientId, io) {
  if (clientId in io.sockets.connected === false) {
    throw new Error(`Unable to find client ${clientId} in connected sockets`)
  }

  return io.sockets.connected[clientId].user
}

module.exports = formatPlayerForFront
