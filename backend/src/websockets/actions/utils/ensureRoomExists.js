const ensureRoomExists = room => {
  if (!room) {
    throw new Error(`Room #${roomId} was not found`)
  }

  return room
}

module.exports = ensureRoomExists
