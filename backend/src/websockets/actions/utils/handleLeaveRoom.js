const { EVT_ROOM_PLAYER_UPDATE, EVT_PLAYER_FLUSH_HAND } = require('../../events')
const { leaveRoom, getRoomPlayersSocketIds } = require('./io')
const formatPlayerForFront = require('./formatPlayerForFront')
const { roomRepository } = require('../../../db/repos')
const logger = require('../../../logger')
const fromLocal = require('./fromLocal')
const slotRotations = require('./slotRotations')

function resetSlot ({ name }) {
  return {
    name,
    userId: null,
    hand: {
      cards: []
    }
  }
}

/**
 * Ensure room integrity when a player leaves a room
 *
 * @param {Socket}    socket
 * @param {Object}    leaved    the player that leaved the room
 * @param {Namespace} namespace
 *
 * @returns {Promise}
 */
const handleLeaveRoom = (socket, leaved, roomId, io) => {
  return leaveRoom(socket, roomId)
    .then(() => getRoomPlayersSocketIds(roomId, io))
    .then(remainingPlayerIds => remainingPlayerIds.map(clientId => formatPlayerForFront(clientId, io)))
    .then(players => {
      logger.debug(`Emitting the updated players list (${players.length}) for #${roomId}`)
      socket.to(roomId).emit(EVT_ROOM_PLAYER_UPDATE, {
        players,
        leaved
      })

      return roomRepository.findById(roomId)
    })
    .then(room => {
      if (!room) {
        throw new Error(`Room "${roomId}" not found`)
      }

      const playerSlotIndex = room.table.slots.findIndex(tableSlot => tableSlot.userId === socket.user.id)
      if (playerSlotIndex < 0) {
        throw new Error(`Slot for player ${socket.user.id} was not found`)
      }
      const handCardIds = room.table.slots[playerSlotIndex].hand.cards
      if (handCardIds.length > 0) {
        // place player hand's cards in front of the leaving player
        const positionCtx = slotRotations[room.table.slots[playerSlotIndex].name]
        const coords = fromLocal({ x: 0, y: 290 }, { x: 0, y: 0 }, positionCtx)
        const rotation = positionCtx

        const releasedCards = handCardIds.map(cardId => {
          const cardIdIndex = room.table.cards.findIndex(card => card.id === cardId)
          room.table.cards[cardIdIndex].visible = true
          room.table.cards[cardIdIndex].isFaceUp = false
          room.table.cards[cardIdIndex].coords = coords
          room.table.cards[cardIdIndex].rotation = rotation

          return room.table.cards[cardIdIndex]
        })

        socket.to(roomId).emit(EVT_PLAYER_FLUSH_HAND, {
          cards: releasedCards,
          coords,
          rotation
        })
      }

      room.table.slots[playerSlotIndex] = resetSlot(room.table.slots[playerSlotIndex])

      return room
    })
    .then(room => roomRepository.replace(room))
    .then(result => {
      // finding any slot which is assigned to a user means that the room is not empty
      const hasRemainingPlayers = result.ops[0].table.slots.find(slot => slot.userId !== null)
      if (!hasRemainingPlayers) {
        logger.info(`[room#${roomId}] No more players. Deleting the room`)
        return roomRepository.delete(roomId)
      }
    })
    .catch(err => {
      logger.error(`[room#${roomId}] Failed to leave room: `, err)
    })
  }

module.exports = handleLeaveRoom
