/**
 * Promisified version of socket.io's `Socket.join()` method.
 *
 * @param {Number} roomId
 */
const joinRoom = async (socket, roomId) =>
  new Promise((resolve, reject) => {
    socket.join(roomId, err => {
      err ? reject(err) : resolve()
    })
  })

/**
 * Promisified version of socket.io's `Socket.leave()` method.
 *
 * @param {Number} roomId
 */
const leaveRoom = async (socket, roomId) =>
  new Promise((resolve, reject) => {
    socket.leave(roomId, err => {
      err ? reject(err) : resolve()
    })
  })

/**
 * Promisified version of socket.io's `Namespace.clients()` method.
 *
 * @param {Number} roomId
 */
const getRoomPlayersSocketIds = async (roomId, io) =>
  new Promise((resolve, reject) => {
    io.in(roomId).clients((err, clientIds) => {
      if (err) {
        reject(err)
        return
      }

      resolve(clientIds)
    })
  })

module.exports = {
  joinRoom,
  leaveRoom,
  getRoomPlayersSocketIds
}
