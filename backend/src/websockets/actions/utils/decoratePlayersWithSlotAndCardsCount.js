const decoratePlayersWithSlotAndCardsCount = (roomPlayers, room) =>
  roomPlayers.map(roomPlayer => {
    const playerSlot = room.table.slots.find(slot => slot.userId === roomPlayer.id)
    roomPlayer.cardsCount = playerSlot.hand.cards.length
    roomPlayer.slot = playerSlot.name

    return roomPlayer
  })

module.exports = decoratePlayersWithSlotAndCardsCount
