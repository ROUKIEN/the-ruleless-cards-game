// ugly paste from the ui package
const fromLocal = (localPosition, center, angle) => {
  const negativeAngle = -angle
  return {
    x: (Math.cos(negativeAngle) * (localPosition.x - center.x) - Math.sin(negativeAngle) * (localPosition.y - center.y)) + center.x,
    y: (Math.sin(negativeAngle) * (localPosition.x - center.x) + Math.cos(negativeAngle) * (localPosition.y - center.y)) + center.y
  }
}

module.exports = fromLocal
