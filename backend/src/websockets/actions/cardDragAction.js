const { EVT_CARD_MOVE } = require('../events')
const logger = require('../../logger')
const ensureRoomExists = require('./utils/ensureRoomExists')

const { roomRepository } = require('../../db/repos')

const cardDragAction = ({ socket }) =>
  ({ roomId, card }) => {
    let cardIndex = -1
    roomRepository.findById(roomId)
      .then(ensureRoomExists)
      .then(theRoom =>{
        if (!theRoom) {
          throw new Error(`Room #${roomId} was not found`)
        }

        cardIndex = theRoom.table.cards.findIndex(aCard => aCard.id === card.id)
        if (cardIndex < 0) {
          throw new Error(`card ${card.id} was not found`)
        }

        theRoom.table.cards[cardIndex].coords = card.coords
        return theRoom
      })
      .then(theRoom => {
        roomRepository.replace(theRoom)
        return theRoom
      })
      .then(theRoom => {
        socket.to(roomId).emit(EVT_CARD_MOVE, {
          card: theRoom.table.cards[cardIndex]
        })
      })
      .catch(err => {
        logger.error(`[room#${roomId}] Failed to drag card ${card.id}: `, err)
      })
  }

module.exports = cardDragAction
