const logger = require('../../logger')
const handleLeaveRoom = require('./utils/handleLeaveRoom')
const formatPlayerForFront = require('./utils/formatPlayerForFront')

const roomLeaveAction = ({ socket, io }) =>
  ({ roomId }) => {
    logger.debug(`${socket.user.username} (#${socket.user.id}) to leave room #${roomId}`)
    const player = formatPlayerForFront(socket.id)

    handleLeaveRoom(socket, player, roomId, io)
      .catch(err => { logger.error('Error while leaving room', err) })
  }

module.exports = roomLeaveAction
