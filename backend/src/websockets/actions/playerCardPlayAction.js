const { roomRepository } = require('../../db/repos')
const { EVT_PLAYER_CARD_PLAY_SUCCESS } = require('../events')
const logger = require('../../logger')

const playerCardPlayAction = ({ socket }) =>
  ({ cardId, roomId, userId, rotation }) => {
    let cardBeingPlayedIndex = -1
    let playerSlotIndex = -1
    roomRepository.findById(roomId)
      .then(room => {
        cardBeingPlayedIndex = room.table.cards.findIndex(tableCard => tableCard.id === cardId)
        if (cardBeingPlayedIndex < 0) {
          throw new Error('Card not found')
        }

        playerSlotIndex = room.table.slots.findIndex(slot => slot.userId === userId)
        if (playerSlotIndex < 0) {
          throw new Error('Player slot not found')
        }

        room.table.slots[playerSlotIndex].hand.cards = room.table.slots[playerSlotIndex].hand.cards.filter(handCardId => handCardId !== cardId)

        room.table.cards[cardBeingPlayedIndex].visible = true
        room.table.cards[cardBeingPlayedIndex].isFaceUp = false
        room.table.cards[cardBeingPlayedIndex].rotation = rotation

        return roomRepository.replace(room)
      })
      .then(result => {
        const room = result.ops[0]
        socket.to(roomId).emit(EVT_PLAYER_CARD_PLAY_SUCCESS, {
          card: room.table.cards[cardBeingPlayedIndex],
          userId,
          cardsCount: room.table.slots[playerSlotIndex].hand.cards.length
        })
        logger.debug(`[room#${roomId}] Player ${userId} played card ${cardId}`)
      })
      .catch(err => {
        logger.error(`[room#${roomId}] User ${userId} failed to play card ${cardId}: `, err)
      })
  }

module.exports = playerCardPlayAction
