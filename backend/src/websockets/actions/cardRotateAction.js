const { EVT_CARD_MOVE } = require('../events')
const ensureRoomExists = require('./utils/ensureRoomExists')
const logger = require('../../logger')

const { roomRepository } = require('../../db/repos')

const cardRotateAction = ({ socket }) =>
  ({ roomId, card }) => {
    let cardIndex = -1
    roomRepository.findById(roomId)
      .then(ensureRoomExists)
      .then(theRoom => {
        cardIndex = theRoom.table.cards.findIndex(aCard => aCard.id === card.id)
        if (cardIndex < 0) {
          throw new Error(`card ${card.id} was not found`)
        }

        theRoom.table.cards[cardIndex].rotation = card.rotation

        return theRoom
      })
      .then(theRoom => roomRepository.replace(theRoom))
      .then(result => {
        const theRoom = result.ops[0]
        socket.to(roomId).emit(EVT_CARD_MOVE, {
          card: theRoom.table.cards[cardIndex]
        })
      })
      .catch(err => {
        logger.error(`[room#${roomId}] Failed to rotate card ${card.id}: `, err)
      })
  }

module.exports = cardRotateAction
