const logger = require('../../logger')
const { roomRepository } = require('../../db/repos')
const { EVT_CARD_FLIPPED } = require('../events')

const cardFlipAction = ({ socket }) =>
  ({ room, cardId }) => {
    let theCardBeingFlippedIndex = -1
    roomRepository.findById(room.id)
      .then(theRoom => {
        if (!theRoom) {
          throw new Error(`Room ${room.id} found`)
        }

        theCardBeingFlippedIndex = theRoom.table.cards.findIndex(aCard => aCard.id === cardId)
        theRoom.table.cards[theCardBeingFlippedIndex].isFaceUp = !theRoom.table.cards[theCardBeingFlippedIndex].isFaceUp

        return theRoom
      })
      .then(theRoom => roomRepository.replace(theRoom))
      .then(() => {
        socket.to(room.id).emit(EVT_CARD_FLIPPED, { cardId })
      })
      .catch(err => {
        logger.error(`Failed to flip card ${cardId}: `, err)
      })
  }

module.exports = cardFlipAction
