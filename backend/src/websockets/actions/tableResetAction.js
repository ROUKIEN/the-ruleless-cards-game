const logger = require('../../logger')
const cards = require('../../../decks/cards.js')
const { roomRepository } = require('../../db/repos')
const { getRoomPlayersSocketIds } = require('./utils/io')
const formatPlayerForFront = require('./utils/formatPlayerForFront')
const decoratPlayersWithSlotAndCardsCount = require('./utils/decoratePlayersWithSlotAndCardsCount')
const { TableCard } = require('../../rooms/table')
const defaultCardCoords = require('../../../decks/defaultCardCoords.js')
const { EVT_TABLE_STATUS, EVT_TABLE_RESET_SUCCESS } = require('../events')

/**
 * Create a new Table from the given one.
 *
 * The created table has the same deck options but the cards are just reset
 *
 * @param {Object} table
 */
function resetCardsFrom (table) {
  table.slots = table.slots.map(slot => {
    slot.hand.cards = []

    return slot
  })

  return table
}

const tableResetAction = ({ socket, io }) =>
  ({ roomId }) => {
    let theRoomPlayers = null
    let theRoom = null
    roomRepository.findById(roomId)
      .then(room => {
        theRoom = room

        const newTable = resetCardsFrom(room.table)

        const deckCards = cards(room.deck).getCards()
        const initialDeck = deckCards.map((card, position) => new TableCard(card, defaultCardCoords, position))

        newTable.cards = initialDeck

        room.table = newTable
      })
      .then(() => getRoomPlayersSocketIds(roomId, io))
      .then(clientIds => clientIds.map(clientId => formatPlayerForFront(clientId, io)))
      .then(players => {
        theRoomPlayers = decoratPlayersWithSlotAndCardsCount(players, theRoom)
      })
      .then(() => roomRepository.replace(theRoom))
      .then(result => {
        io.to(roomId).emit(EVT_TABLE_STATUS, {
          table: result.ops[0].table,
          players: theRoomPlayers
        })
        socket.to(roomId).emit(EVT_TABLE_RESET_SUCCESS, {
          user: formatPlayerForFront(socket.id, io)
        })
      })
      .then(() => { logger.debug(`[room#${roomId}] Table reset`) })
      .catch(err => { logger.error(`Error while resetting table: `, err)  })
  }

module.exports = tableResetAction
