const { EVT_POINTER_MOVED } = require('../events')

/**
 * Notifies every player in the room that a player has moved its pointer
 *
 * @param {Object} socket
 * @param {Object} io
 */
const pointerMoveAction = ({ io }) =>
  ({ pointer, room, user }) => {
    io.to(room.id).emit(EVT_POINTER_MOVED, {
      pointer,
      user
    })
  }

module.exports = pointerMoveAction
