const logger = require('../../logger')
const { roomRepository } = require('../../db/repos')
const { EVT_PLAYER_CARD_DRAW_SUCCESS } = require('../events')

const playerCardDrawAction = ({ socket }) =>
  ({ cardId, userId, roomId }) => {
    let theCardBeingDrawnIndex = -1
    let playerSlotIndex = -1

    roomRepository.findById(roomId)
      .then(room => {
        theCardBeingDrawnIndex = room.table.cards.findIndex(tableCard => tableCard.id === cardId)
        if (theCardBeingDrawnIndex < 0) {
          throw new Error(`Card ${cardId} not found`)
        }

        playerSlotIndex = room.table.slots.findIndex(slot => slot.userId === userId)

        room.table.slots[playerSlotIndex].hand.cards.push(room.table.cards[theCardBeingDrawnIndex].id)
        room.table.cards[theCardBeingDrawnIndex].visible = false
        room.table.cards[theCardBeingDrawnIndex].isFaceUp = false

        return roomRepository.replace(room)
      })
      .then(result => {
        const room = result.ops[0]
        socket.to(roomId).emit(EVT_PLAYER_CARD_DRAW_SUCCESS, {
          cardId,
          userId,
          cardsCount: room.table.slots[playerSlotIndex].hand.cards.length
        })
        logger.debug(`[room#${roomId}] Player ${userId} drawn card ${cardId}`)
      })
      .catch(err => {
        logger.error(`[room#${roomId}] Failed to draw card ${cardId} into player ${userId} hand: `, err)
      })
  }

module.exports = playerCardDrawAction
