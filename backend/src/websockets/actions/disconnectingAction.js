const logger = require('../../logger')
const formatPlayerForFront = require('./utils/formatPlayerForFront')
const handleLeaveRoom = require('./utils/handleLeaveRoom')

const disconnectingAction = ({ socket, io }) =>
  () => {
    const player = formatPlayerForFront(socket.id, io)

    Object.keys(socket.rooms)
      .filter(roomId => roomId !== socket.id) // there is a room per client connection whose id is the socket id
      .forEach(roomId => {
        logger.debug(`Removing ${socket.user.username} from ${roomId}`)
        handleLeaveRoom(socket, player, roomId, io)
          .then(() => { logger.debug(`Removed user #${player.username} from room #${roomId}`)})
          .catch(error => {
            logger.error('Error while leaving room: ', error)
          })
      })
    }

module.exports = disconnectingAction
