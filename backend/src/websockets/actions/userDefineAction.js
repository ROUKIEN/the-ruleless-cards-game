const { EVT_USER_DEFINE_FAIL, EVT_USER_DEFINE_SUCCESS } = require('../events')

const userDefine = ({ socket }) =>
  ({ user }) => {
    if (!user) {
      socket.emit(EVT_USER_DEFINE_FAIL)
      socket.disconnect()
      return
    }

    socket.user = user
    socket.emit(EVT_USER_DEFINE_SUCCESS, { user })
  }

module.exports = userDefine
