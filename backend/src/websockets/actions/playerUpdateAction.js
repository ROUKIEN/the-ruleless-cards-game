const logger = require('../../logger')

const playerUpdateAction = ({ io }) =>
  ({ user, roomId }) => {
    io.to(roomId).emit('player:update:success', { user })
    logger.debug(`[room#${roomId}] Updating player "${user.id}" infos`)
  }

module.exports = playerUpdateAction
