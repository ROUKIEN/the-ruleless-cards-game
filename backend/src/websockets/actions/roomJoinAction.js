const logger = require('../../logger')
const { joinRoom, getRoomPlayersSocketIds } = require('./utils/io')
const {
  EVT_ROOM_UNKNOWN,
  EVT_ROOM_JOINED,
  EVT_ROOM_JOIN_FAIL,
  EVT_TABLE_STATUS,
  EVT_ROOM_PLAYER_UPDATE
} = require('../events')
const formatPlayerForFront = require('./utils/formatPlayerForFront')
const decoratePlayersWithSlotAndCardsCount = require('./utils/decoratePlayersWithSlotAndCardsCount')
const { roomRepository } = require('../../db/repos')

/**
 * Affects a slot to a given player
 *
 * @param {Object} room
 * @param {String} playerId
 */
function join (room, playerId) {
  const availableSlotIndex = room.table.slots.findIndex(slot => slot.userId === null)
  if (availableSlotIndex === -1) {
    throw new Error('Table does not contain any available slots.')
  }

  room.table.slots[availableSlotIndex].userId = playerId

  return room.table.slots[availableSlotIndex]
}

/**
 * When a player joins a table, we try to assign the player an available slot
 *
 * @param {Object} socket
 * @param {Object} io
 */
const roomJoinAction = ({ socket, io }) =>
  ({ roomId }) => {
    logger.debug(`${socket.user.username} to join room #${roomId}`)

    let room = null
    let slot = null
    let roomPlayers = null

    roomRepository.findById(roomId)
      .then(theRoom => {
        if (!theRoom) {
          socket.emit(EVT_ROOM_UNKNOWN, { roomId })
          throw new Error(`Room ${roomId} not found`)
        }
        room = theRoom

        return joinRoom(socket, roomId)
      })
      .then(() => { slot = join(room, socket.user.id) })
      .then(() => getRoomPlayersSocketIds(room.id, io))
      .then(clientIds => clientIds.map(clientId => formatPlayerForFront(clientId, io)))
      .then(players => {
        logger.debug(`[room#${roomId}] ${players.length} players`)
        roomPlayers = players

        return roomRepository.replace(room)
      })
      .then(result => {
        const room = result.ops[0]
        roomPlayers = decoratePlayersWithSlotAndCardsCount(roomPlayers, room)
      })
      .then(() => {
        socket.emit(EVT_ROOM_JOINED, {
          players: roomPlayers,
          slot
        })
      })
      .then(() => {
        socket.to(roomId).emit(EVT_ROOM_PLAYER_UPDATE, {
          players: roomPlayers,
          entered: formatPlayerForFront(socket.id, io),
          slot: slot.name
        })
      })
      .then(() => {
        socket.emit(EVT_TABLE_STATUS, {
          table: room.table,
          players: roomPlayers
        })
      })
      .catch(err => {
        const reason = `"${socket.user.username}" failed to join room #${roomId}`
        logger.error(`${reason}: `, err)
        socket.emit(EVT_ROOM_JOIN_FAIL, { reason })
      })
  }

module.exports = roomJoinAction
