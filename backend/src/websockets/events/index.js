const EVT_ROOM_PLAYER_UPDATE = 'room:players:update'
const EVT_ROOM_JOIN = 'room:join'
const EVT_ROOM_JOINED = 'room:joined'
const EVT_ROOM_JOIN_FAIL = 'room:join:fail'
const EVT_ROOM_LEAVE = 'room:leave'
const EVT_ROOM_UNKNOWN = 'room:unknown'
const EVT_USER_DEFINE = 'user:define'
const EVT_USER_DEFINE_SUCCESS = 'user:define:success'
const EVT_USER_DEFINE_FAIL = 'user:define:fail'
const EVT_TABLE_STATUS = 'table:status'
const EVT_CARD_DRAG = 'card:drag'
const EVT_CARD_MOVE = 'card:move'
const EVT_CARD_FLIP = 'card:flip'
const EVT_CARD_FLIPPED = 'card:flipped'
const EVT_POINTER_MOVE = 'pointer:move'
const EVT_POINTER_MOVED = 'pointer:moved'
const EVT_PLAYER_CARD_DRAW = 'player:card:draw'
const EVT_PLAYER_CARD_DRAW_SUCCESS = 'player:card:draw:success'
const EVT_PLAYER_CARD_PLAY = 'player:card:play'
const EVT_PLAYER_CARD_PLAY_SUCCESS = 'player:card:play:success'
const EVT_PLAYER_FLUSH_HAND = 'player:flush-hand'
const IO_DISCONNECTING = 'disconnecting'
const EVT_CARD_ROTATE = 'card:rotate'
const EVT_TABLE_RESET = 'table:reset'
const EVT_TABLE_RESET_SUCCESS = 'table:reset:success'
const EVT_PLAYER_UPDATE = 'player:update'
const EVT_PLAYER_UPDATE_SUCCESS = 'player:update:success'

module.exports = {
  EVT_ROOM_PLAYER_UPDATE,
  EVT_ROOM_JOIN,
  EVT_ROOM_JOINED,
  EVT_ROOM_JOIN_FAIL,
  EVT_ROOM_LEAVE,
  EVT_ROOM_UNKNOWN,
  EVT_USER_DEFINE,
  EVT_USER_DEFINE_SUCCESS,
  EVT_USER_DEFINE_FAIL,
  EVT_TABLE_STATUS,
  EVT_CARD_DRAG,
  EVT_CARD_MOVE,
  EVT_CARD_FLIP,
  EVT_CARD_FLIPPED,
  EVT_POINTER_MOVE,
  EVT_POINTER_MOVED,
  EVT_PLAYER_CARD_DRAW,
  EVT_PLAYER_CARD_DRAW_SUCCESS,
  EVT_PLAYER_CARD_PLAY,
  EVT_PLAYER_CARD_PLAY_SUCCESS,
  EVT_PLAYER_FLUSH_HAND,
  IO_DISCONNECTING,
  EVT_CARD_ROTATE,
  EVT_TABLE_RESET,
  EVT_TABLE_RESET_SUCCESS,
  EVT_PLAYER_UPDATE,
  EVT_PLAYER_UPDATE_SUCCESS,
}
