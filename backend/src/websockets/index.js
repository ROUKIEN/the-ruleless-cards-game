const ws = require('socket.io')
const logger = require('../logger')

const userDefineAction = require('./actions/userDefineAction.js')
const cardFlipAction = require('./actions/cardFlipAction.js')
const roomLeaveAction = require('./actions/roomLeaveAction.js')
const roomJoinAction = require('./actions/roomJoinAction.js')
const cardDragAction = require('./actions/cardDragAction.js')
const pointerMoveAction = require('./actions/pointerMoveAction.js')
const playerCardPlayAction = require('./actions/playerCardPlayAction.js')
const playerCardDrawAction = require('./actions/playerCardDrawAction.js')
const disconnectingAction = require('./actions/disconnectingAction.js')
const cardRotateAction = require('./actions/cardRotateAction.js')
const tableResetAction = require('./actions/tableResetAction.js')
const playerUpdateAction = require('./actions/playerUpdateAction.js')

const {
  EVT_USER_DEFINE,
  EVT_ROOM_JOIN,
  EVT_ROOM_LEAVE,
  EVT_CARD_FLIP,
  EVT_CARD_DRAG,
  EVT_POINTER_MOVE,
  EVT_PLAYER_CARD_PLAY,
  EVT_PLAYER_CARD_DRAW,
  IO_DISCONNECTING,
  EVT_CARD_ROTATE,
  EVT_TABLE_RESET,
  EVT_PLAYER_UPDATE
} = require('./events')

const EVENTS = {
  [EVT_USER_DEFINE]: userDefineAction,
  [EVT_ROOM_JOIN]: roomJoinAction,
  [EVT_ROOM_LEAVE]: roomLeaveAction,
  [EVT_CARD_FLIP]: cardFlipAction,
  [EVT_CARD_DRAG]: cardDragAction,
  [EVT_POINTER_MOVE]: pointerMoveAction,
  [EVT_PLAYER_CARD_PLAY]: playerCardPlayAction,
  [EVT_PLAYER_CARD_DRAW]: playerCardDrawAction,
  [EVT_CARD_ROTATE]: cardRotateAction,
  [IO_DISCONNECTING]: disconnectingAction,
  [EVT_TABLE_RESET]: tableResetAction,
  [EVT_PLAYER_UPDATE]: playerUpdateAction
}

function createWS (server) {
  const io = ws(server)

  io.on('connection', socket => {
    logger.debug(`a user connected, ${Object.keys(io.sockets.connected).length} current connections`)

    Object.keys(EVENTS).forEach(EVENT => {
      socket.on(EVENT, EVENTS[EVENT]({ socket, io }))
    })

    socket.on('disconnect', () => {
      logger.debug(`a user logged out, ${Object.keys(io.sockets.connected).length} remaining connections`)
    })
  })

  return io
}

module.exports = {
  createWS
}
