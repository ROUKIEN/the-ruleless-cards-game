const db = require('../db')

class RoomRepository {
  findById (id, applyProjection = false) {

    const projection = applyProjection === true ? {
      _id: 0,
      table: 0
    } : {}

    return this.collection()
      .findOne({ id }, { projection })
  }

  save (room) {
    return this.collection()
      .insertOne(room)
  }

  replace (room) {
    return this.collection()
      .replaceOne({ id: room.id }, room)
  }

  delete (id) {
    return this.collection()
      .deleteOne({ id })
  }

  collection () {
    return db.collection('room')
  }
}

module.exports = RoomRepository
