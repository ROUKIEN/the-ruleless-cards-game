const { MongoClient } = require('mongodb')

let client = null
let connection = null
let dbName = 'trcg'

const db = {
  async connect (url, theDbName) {
    dbName = theDbName
    client = new MongoClient(url, { useNewUrlParser: true, useUnifiedTopology: true })
    try {
      connection = await client.connect()

      return connection
    } catch (err) {
      throw err
    }
  },
  disconnect () {
    client && client.close()
  },
  connection () {
    return connection
  },
  db () {
    return connection.db(dbName)
  },
  collection (collectionName) {
    return connection.db(dbName).collection(collectionName)
  }
}

module.exports = db
