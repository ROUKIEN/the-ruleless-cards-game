const winston = require('winston')

const logger = winston.createLogger({
  level: process.env.NODE_ENV === 'development' ? 'debug' : 'info',
  format: winston.format.json(),
  transports: [
    new winston.transports.Console()
  ]
})

if (process.env.NODE_ENV === 'development') {
  logger.clear()
  logger.add(new winston.transports.Console({
    format: winston.format.simple({
      color: true
    })
  }))
}

module.exports = logger
