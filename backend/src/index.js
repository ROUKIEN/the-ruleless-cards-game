const http = require('http')
const redis = require('socket.io-redis')
const Sentry = require('@sentry/node')
const { createWS } = require('./websockets')
const { createAPI } = require('./api')
const db = require('./db/db.js')
const logger = require('./logger')
const SENTRY_DSN = process.env.SENTRY_DSN || false

if (SENTRY_DSN) {
  const SENTRY_ENV_NAME = process.env.SENTRY_ENV_NAME || 'undefined'
  logger.debug(`setting sentry dsn for environment ${SENTRY_ENV_NAME}`)
  Sentry.init({
    dsn: SENTRY_DSN,
    environment: SENTRY_ENV_NAME
  })
}

const PORT = process.env.PORT || 3000

const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017'
const dbName = 'trcg'

db.connect(mongoUrl, dbName)
  .then(() => {
    logger.info('connected to db')

    const api = createAPI(SENTRY_DSN === true)
    const server = http.createServer(api)
    const io = createWS(server)

    if (process.env.ADAPTER_NAME === 'redis') {
      const REDIS_HOST = process.env.ADAPTER_REDIS_HOST || 'localhost'
      const REDIS_PORT = process.env.ADAPTER_REDIS_PORT || 6379

      io.adapter(redis({
        host: REDIS_HOST,
        port: REDIS_PORT
      }))
    }

    server.listen(PORT, () => {
      logger.info(`server is ready at :${PORT}`)
    })
  })
  .catch(err => {
    logger.error('Failed to connect to db: ', err)
    process.exit(1)
  })
