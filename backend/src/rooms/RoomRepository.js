class RoomRepository {
  constructor () {
    this.rooms = {}
  }

  save (room) {
    this.rooms[room.id] = room
  }

  /**
   * Retrieve a room by the room Id
   * @param {Number} roomId
   */
  find (roomId) {
    return this.rooms[roomId]
  }

  /**
   * Deletes the room
   * @param {Number} roomId
   */
  delete (roomId) {
    delete this.rooms[roomId]
  }

  /**
   * Updating a table card will put it at the top position of the card table so that it will be visible.
   *
   * @param {Number} roomId
   * @param {Object} card
   */
  update (roomId, card) {
    const room = this.rooms[roomId]
    const theCardBeeingUpdatedIndex = room.table.cards.findIndex(currentCard => currentCard.id === card.id)

    card.position = ++ room.table.maxPosition

    room.table.cards[theCardBeeingUpdatedIndex] = card

    return card
  }

  drop () {
    this.rooms = {}
  }
}

module.exports = RoomRepository
