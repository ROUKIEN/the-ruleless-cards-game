const { v4 } = require('uuid')

class TableCard {
  constructor (card, coords, position) {
    this.id = v4()
    this.card = card
    this.coords = coords
    this.position = position
    this.rotation = 0
    this.isFaceUp = false
    this.visible = true
  }
}

module.exports = TableCard
