const Slot = require('./Slot')

const slotPositions = [
  'bottom',
  'top',
  'left',
  'right'
]

/**
 * A Table is a current game state. It contains cards & their positions, slots for players...
 */
class Table {
  constructor (cards = []) {
    this.slots = slotPositions.map(slotPosition => new Slot(slotPosition))
    this.cards = cards
    this.maxPosition = Math.max(cards.length -1, 0)
  }

  findCard (card) {
    return this.cards.findIndex(currentCard => currentCard.id === card.id)
  }

  setCard (card, index) {
    this.cards[index] = card
  }

  getCard (index) {
    return this.cards[index] || null
  }

  join (userId) {
    const availableSlot = this.slots.find(slot => slot.isAvailable())
    if (!availableSlot) {
      throw new Error('Table does not contain any available slots.')
    }

    availableSlot.assign(userId)

    return availableSlot
  }

  leave (userId) {
    const userSlot = this.slots.find(slot => userId === slot.getUserId())
    if (userSlot) {
      userSlot.release()
    }
  }

  getPlayerSlot(userId) {
    return this.slots.find(tableSlot => userId === tableSlot.getUserId())
  }
}

module.exports = Table
