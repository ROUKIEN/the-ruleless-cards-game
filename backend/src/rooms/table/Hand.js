class Hand {
  constructor () {
    this.cards = []
  }

  add (cardId) {
    const exists = this.cards.find(handCard => handCard === cardId)
    if (!exists) {
      this.cards.push(cardId)
    }
  }

  remove (cardId) {
    this.cards = this.cards.filter(handCard => handCard !== cardId)
  }

  isEmpty () {
    return this.cards.length === 0
  }

  countCards () {
    return this.cards.length
  }
}

module.exports = Hand
