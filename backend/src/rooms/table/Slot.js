const Hand = require('./Hand')

/**
 * A Slot is a player position at a table.
 */
class Slot {
  constructor (name, hand = new Hand()) {
    this.userId = null
    this.name = name
    this.hand = hand
  }

  getUserId () {
    return this.userId
  }

  isAvailable () {
    return this.userId === null
  }

  assign (userId) {
    this.userId = userId
  }

  release () {
    this.userId = null
    this.hand = new Hand()
  }

  addCard (cardId) {
    this.hand.add(cardId)
  }

  removeCard (cardId) {
    this.hand.remove(cardId)
  }
}

module.exports = Slot
