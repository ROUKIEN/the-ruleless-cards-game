# legacy vars
DIR_FRONTEND=./frontend
DIR_BACKEND=./backend
DIR_DIST=./dist

# here comes the true things

CMD_NPM=npm
CMD_DOCKER_COMPOSE=docker-compose

SVC_UI=ui
SVC_SERVER=server

ENV=$(CI_ENV)
ifeq ($(ENV),ci)
    NPM_INSTALL_FLAG=ci
	TEST_FLAGS=--ci --reporters=default --reporters=jest-junit
else
	NPM_INSTALL_FLAG=install
	TEST_FLAGS=--reporters=default

	include .env
endif

ifeq ($(EXECUTOR),docker)
	RUN_IN_UI=$(CMD_DOCKER_COMPOSE) run $(SVC_UI)
	RUN_IN_SERVER=$(CMD_DOCKER_COMPOSE) run $(SVC_SERVER)
	# jest need git to run the --watch flag. It is not installed in the node docker images
	FLAGS_WATCH=--watchAll
else
	RUN_IN_UI=cd $(DIR_FRONTEND); 
	RUN_IN_SERVER=cd $(DIR_BACKEND);
	FLAGS_WATCH=--watch
endif

# targets

.PHONY: configure
configure: .env
	$(shell echo U_ID=`id -u` >> .env)
	$(shell echo G_ID=`id -g` >> .env)
	$(shell echo MONGO_PWD=`cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1` >> .env)

.env:
	cp .env.dist .env

.PHONY: install ## installs all packages dependencies
install: install-frontend install-backend
	
.PHONY: install-frontend
install-frontend: ## installs ui dependencies
	$(RUN_IN_UI) $(CMD_NPM) $(NPM_INSTALL_FLAG)

.PHONY: install-backend
install-backend: ## installs backend dependencies
	$(RUN_IN_SERVER) $(CMD_NPM) $(NPM_INSTALL_FLAG)

.PHONY: test-frontend
test-frontend: ## unit tests frontend
	$(RUN_IN_UI) $(CMD_NPM) run test:unit -- $(TEST_FLAGS)

.PHONY: test-frontend-watch
test-frontend-watch: ## rerun frontend unit tests at every code change
	$(RUN_IN_UI) $(CMD_NPM) run test:unit -- $(FLAGS_WATCH)

.PHONY: test-backend
test-backend: ## unit tests backend
	$(RUN_IN_SERVER) $(CMD_NPM) run test -- $(TEST_FLAGS)

.PHONY: test-backend-watch
test-backend-watch: ## rerun backend unit tests at every code change
	$(RUN_IN_SERVER) $(CMD_NPM) run test -- $(FLAGS_WATCH)

.PHONY: build
build: clean ## builds the frontend, the backend then make the backend serve the frontend.
	mkdir $(DIR_DIST)
	cd $(DIR_FRONTEND); \
	  echo "VUE_APP_SENTRY_DSN=$(SENTRY_DSN)" >> .env.local; \
	  echo "VUE_APP_SENTRY_ENV_NAME=$(SENTRY_ENV_NAME)" >> .env.local; \
	  $(CMD_NPM) ci; \
	  $(CMD_NPM) run build
	cd $(DIR_BACKEND); \
	  $(CMD_NPM) ci
	cp -r $(DIR_BACKEND)/* $(DIR_DIST)
	cp -r $(DIR_FRONTEND)/dist $(DIR_DIST)

.PHONY: dev
dev: ## starts the project in dev mode
	$(CMD_DOCKER_COMPOSE) up -d

.PHONY: stop
stop: ## stops the project
	$(CMD_DOCKER_COMPOSE) stop

.PHONY: restart
restart: ## restarts the stack
	$(CMD_DOCKER_COMPOSE) restart

.PHONY: logs
logs: ## tails project logs
	$(CMD_DOCKER_COMPOSE) logs -f

.PHONY: clean
clean: ## delete the .env file
	rm -f .env
	rm -rf $(DIR_DIST)

mrproper: stop clean ## stops, cleans & remove the docker-compose images
	$(CMD_DOCKER_COMPOSE) rm -s -v -f